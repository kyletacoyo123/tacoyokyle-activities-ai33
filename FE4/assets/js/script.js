Chart.defaults.global.defaultFontFamily='Lato';
var ctx = document.getElementById('graph').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
          "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        ],
        datasets: [
      {
        label: "Returned",
        data: [12, 19, 3, 5, 2, 3, 5, 2],
          backgroundColor: [
                'rgb(0, 0, 204)',
                'rgb(0, 0, 204)',
                'rgb(0, 0, 204)',
                'rgb(0, 0, 204)',
                'rgb(0, 0, 204)',
                'rgb(0, 0, 204)',
                'rgb(0, 0, 204)',
                'rgb(0, 0, 204)',        
            ],
      },
      {
        label: "Burrowed",
        hoverBackgroundColor: "red",
        data: [15, 26, 2, 6, 8, 6, 3, 3],
        backgroundColor:[
                ' rgb(255, 153, 0)',
                ' rgb(255, 153, 0)',
                ' rgb(255, 153, 0)',
                ' rgb(255, 153, 0)',
                ' rgb(255, 153, 0)',
                ' rgb(255, 153, 0)',
                ' rgb(255, 153, 0)',
                ' rgb(255, 153, 0)',
        ]
      },
    ],
  },
    options: {

          title:{
            display:true,
            text:'Borrowed and Returned Books'
          },

          legend:{
            position:'bottom'
          }
       

    }
});
var ctx = document.getElementById('pie').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Book1', 'Book2', 'Book3'],
        datasets: [{
            data: [12, 19, 3],
            backgroundColor: [
                'rgba(144, 19, 254, 1)',
                'rgba(0, 122, 255, 1)',
                'rgba(230, 229, 230, 1)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
       
        title:{
            display:true,
            text:'MOST POPULAR BOOKS'
          },

          legend:{
            position:'bottom'
          }
    }
});
