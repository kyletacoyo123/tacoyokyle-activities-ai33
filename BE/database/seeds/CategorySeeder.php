<?php


use App\Models\category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mul_rows= [
            [ 'category' => 'Horror'],
            [ 'category' => 'Fantasy'],
            [ 'category' => 'Romance'],
            [ 'category' => 'History'],
            [ 'category' => 'Science Fiction']
        ];
        
            foreach ($mul_rows as $rows) {
            category::create($rows);
                }
    }
}
