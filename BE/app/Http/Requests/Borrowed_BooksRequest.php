<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Borrowed_BooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patron_id' => 'integer|required',
            'copies' => 'integer|max:100|required',
            'book_id' => 'integer|required'
        ];
    }

}
