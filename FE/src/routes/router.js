import Vue from "vue";
import VueRouter from "vue-router";
import dashboard from "../pages/dashboard.vue";
import patron from "../pages/patron.vue";
import book from "../pages/book.vue";
import settings from "../pages/settings.vue";


Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            component: dashboard,

        },
        {
            path: "/dashboard",
            name: "dashboard",
            component: dashboard, 

        },
        {
            path: "/patron",
            name: "patron",
            component: patron,
        },
        {
            path: "/book",
            name: "book",
            component: book,
        },
        {
            path: "/settings",
            name: "settings",
            component: settings,
        },

    ],
});