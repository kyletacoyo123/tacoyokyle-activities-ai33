import axios from "axios";
const resourse_uri = "http://127.0.0.1:8000/api/Categories";

const state = {
  Categories: [],
};
const getters = {
  AllCategories: (state) => state.Categories,
};
const actions = {
  async fetchCategory({ commit }) {
    const response = await axios.get(resourse_uri);
    commit("Categories", response.data.data);
  },
};
const mutations = {
  Categories: (state, Categories) => (state.Categories = Categories),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
