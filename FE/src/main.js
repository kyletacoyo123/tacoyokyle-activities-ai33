import Vue from 'vue';
import router from "./routes/router.js";
import App from "../src/App.vue";
import Toast from "vue-toastification";
import store from "./store/store";
import  VueFilterDateFormat from "vue-filter-date-format";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import Vuelidate from 'vuelidate';


Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 7,
  newestOnTop: true,
});

import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.min.css"
import "./assets/css/style.css"
import "vue-toastification/dist/index.css";
Vue.config.productionTip = false;

new Vue({  
  store,
  router,
  render: (h) => h(App),
}).$mount('#app');

